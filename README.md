# SPHN reporting

This repository contains necessary templates and scripts to facilitate projects for reporting
[qualitative metadata](./Qualitative%20Metadata) and [quantitative metadata](./Quantitative%20Metadata). Both information will be used to represent datasets on the [SPHN Metadata Catalog](https://fdp-schemascope.dcc.sib.swiss/)