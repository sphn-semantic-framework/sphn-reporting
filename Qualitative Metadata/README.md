# Instructions for reporting Qualitative Metadata

The [SPHN_metadata_catalogue_template_v2.0.xlsx](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-reporting/-/blob/main/Qualitative%20Metadata/SPHN_metadata_catalog_template_v2.0.xlsx) is a template Excel provided
by the SPHN DCC for reporting qualitative metadata about a project.

Projects should use this template as a starting point for providing metadata as part of their reporting requirements.

Exact instructions for reporting are provided directly to projects by the SPHN DCC. With submitting descriptive metadata to the DCC you agree with the information beeing published on the [SPHN Metadata Catalog](https://fdp.dcc.sib.swiss/).

