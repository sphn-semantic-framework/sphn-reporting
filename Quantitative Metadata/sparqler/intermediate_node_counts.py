""" This module contains the needed functionality to account for intermediate node counts """

import os
from typing import Any, Dict, List, Optional

from rdflib import URIRef

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))


'''def format_prefix(uri) -> str:
    path = str(uri)
    for prefix in ['https://biomedit.ch/rdf/sphn-schema/sphn#', 'https://biomedit.ch/rdf/sphn-ontology/sphn#', 'https://biomedit.ch/rdf/sphn-schema/nexus#']:
        if prefix in path:
            if '/sphn#' in path and '/sphn#' in prefix:
                return path.replace(prefix, '/sphn:')
            elif '/nexus#' in path and '/nexus#' in prefix:
                return path.replace(prefix, '/nexus:')
        else:
            return f'/<{path}>'''


def format_prefix(uri, prefixes):
    dic = {'https://biomedit.ch/rdf/sphn-schema/sphn#': '/sphn:', 
           'https://biomedit.ch/rdf/sphn-ontology/sphn#': '/sphn:', 
           'https://biomedit.ch/rdf/sphn-schema/nexus#' : '/nexus:'
           }
    path = str(uri)
    for i, j in prefixes.items():
        path = path.replace(j, f"/{i}:")
    return path


def simplify_object_properties(data_properties:dict, prefixes:dict) -> dict[str, URIRef]:
    """
    simplify_data_properties formats the keys of the data_properties dict

    for easier access in the 'render_count' function

    Args:
        data_properties (dict): dict of valid data properties

    Returns:
        dict: with formatted properties as keys and valid properties as values
    """
    formatted_dict = {}
    for key, values in data_properties.items():
        formatted_dict[format_prefix(key, prefixes).lstrip("/")] = values
    return formatted_dict


def create_context_for_jinja(concept, path, property_path, server_type, type_query:Optional[Dict[str, str]], graph:Optional[str] = None, sphn_prefix:Optional[str] = None, datatype:Optional[str] = None) -> Dict[str, str]:
    """
    create_context_for_jinja

    creates context variable for the jinja rendering

    Args:
        concept (str): bane if the concept
        path (star): path currently checked
        property_path (str): path in fully expanded IRI form
        server_type (str): server type
        type_query (dict): result from dict from 'check_intermediate_nodes' func
        graph (str, optional): Type of backend graph. Defaults to None.

    Returns:
        dict[str, Any]: context for jinja template
    """
    context = {"Concept":concept, "path":path, "property_path":property_path, "server_type":server_type, "sphn_prefix": sphn_prefix, "datatype":datatype}

    # since type_query argument is of type dict we can directly use it to update
    # the context variable.
    if type_query:
        context |= type_query

    if graph:
        context["graph"] = graph

    return context


def check_intermediate_nodes(path, target, concept, object_properties, restrictions, prefixes, root_nodes) -> Optional[List[Dict[str, str]]]:
    """
    Adapts the code for the SPARQL Queries for the MetaDataGenerator to
    also acount fo intermediate node counts.

    Args:
        path (str): path to target
        object_properties (dict): dict of all object properties and ranges

    Returns:
        list[dict[str, str]]: returns list of dicts to check either for Code or terminology
    """
    prefixed_root_nodes = [format_prefix(root, prefixes).lstrip("/") for root in root_nodes]

    simplify_concept = format_prefix(concept, prefixes).lstrip("/")
    if len(path) > 1:
        attribute = path.split("/")[-1]
    else:
        attribute = path

    # it is needed to check for restrictions on the context of the node itself.

    restricted_properties = restrictions.get(f"{simplify_concept}/{attribute}")
    object_range = object_properties.get(attribute)
    
    # in case there is an restriction ont he concept attribute combination
    # we need to check only for the restriction

    # in the case the object_range is a SPHNConcept (or project root), we must check the restricted_properties for the path_end and assign the restrictions from there.
    # this is especially neccessary for the Valuesets

    if len(object_range or '') == 1 and restricted_properties == None:
        object_rangesimple = object_range["ranges"][0]
        if object_range["ranges"][0] in root_nodes:
            key_restriction_by_property = []
            for keys in restrictions.keys():
                if keys.endswith(attribute):               
                    key_restriction_by_property.append(keys)
            restricted_properties = []
            for key in key_restriction_by_property :
                for restriction in restrictions[key]:
                    property_before_current_in_path = path.split("/"+attribute)[0].split("/")[-1]
                    classNameForRestriction = key.split("/")[0].split(":")[-1]
                    if classNameForRestriction in property_before_current_in_path:
                        restricted_properties.append(restriction)

    if target is not None and not target.startswith("http://www.w3.org/2001/XMLSchema#"):
        valid_data_types = [format_prefix(target, prefixes).lstrip("/")]
        result = create_result_list_dict(valid_data_types=valid_data_types, prefixed_root_nodes=prefixed_root_nodes)
        return result
    elif restricted_properties:
        valid_data_types = [format_prefix(prop, prefixes).lstrip("/") for prop in restricted_properties]
        result = create_result_list_dict(valid_data_types=valid_data_types, prefixed_root_nodes=prefixed_root_nodes)
        return result
    elif object_range:
        valid_data_types = [format_prefix(prop, prefixes).lstrip("/") for prop in object_range["ranges"]]
        result = create_result_list_dict(valid_data_types=valid_data_types, prefixed_root_nodes=prefixed_root_nodes)
        return result
    else:
        return None


def create_result_list_dict(valid_data_types:List, prefixed_root_nodes:List)-> List[Dict[str,str]]:
    """
    create_result_list_dict returns the (list) of dictionaries needed for the 
    templated SPARQL queries.

    In case the target is of type Terminology we need to check for the rdfs:subclassOf*
    Otherwise we check for the rdf:type & sphn:Code
    In case we find both as valid targets we need to check for both.
    If we find multiple "Code" or "multiple" Terminology target we
    still return at maximum a list of two dictionaries.

    Args:
        valid_data_types (list): list of valid data types
        prefixed_root_nodes (list): list of root nodes (prefixed)

    Returns:
        list[Dict]: result dict used for templated sparql query
    """

    result_list = []
    for i in valid_data_types:
        if i == "sphn:Terminology" or i in prefixed_root_nodes:
            result_list.append({"type_query" : r"rdf:type/rdfs:subClassOf*", "type": i})
        else:
            result_list.append({"type_query" : r"rdf:type", "type": i})
    return result_list
