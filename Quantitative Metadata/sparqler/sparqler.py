"""
Sparqler Queries Generator.

This script generates sparql queries for a given sphn/project schema.

Created: May 2024
"""
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from intermediate_node_counts import check_intermediate_nodes, create_context_for_jinja, format_prefix, simplify_object_properties
from rml_generator_lib import load_graph, simplify_schema_graph, get_restrictions, get_restrictions_map, niceify_prefix
from rdflib import URIRef, RDF, RDFS, BNode, OWL, Graph
from jinja2 import Environment, FileSystemLoader
from jinja2.environment import Template
import logging
import typing
from pathlib import Path
import argparse
from datetime import datetime
from typing import Optional

# target concepts of terminology remove
# TODO: check that only sphn and project prefix is sent. 
# TODO: check that only sphn and project prefix is sent ro replacements. i.e detect proejct and sphn prefixes

SPHN = "https://biomedit.ch/rdf/sphn-schema/sphn"
ONTOLOGY = "https://biomedit.ch/rdf/sphn-ontology/sphn"
SPHN_NAMESPACE = str()  # replaced later by either SPHN or ONTOLOGY (conditional)
DEPRECATED_SPHN_PREFIX = "https://biomedit.ch/rdf/sphn-schema/sphn#Deprecated"
GLOBAL_PREFIXES = []
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
DEFAULT_OUTPUT_DIR = os.path.join(SCRIPT_DIR, "sparql-queries")
DEFAULT_TEMPLATES_DIR = os.path.join(SCRIPT_DIR, "templates")
DIR_TEMPLATE_MAPPING = {'concept_flattening': 'flattening.rq',
                        'count_instances': 'count.rq',
                        'hasCode': 'hasCode.rq',
                        'min_max_predicates': 'min_max.rq',
                        }
TERMINOLOGY_PREFIX_EXCLUSIONS = ['xml', 'foaf', 'owl', 'rdf', 'rdfs', 'xsd', 'skos', 'sphn']


def setup_logger():
    log_dir = os.path.join(SCRIPT_DIR, 'logs')
    os.makedirs(log_dir, exist_ok=True)
    logs = logging.getLogger("SPARQLer logger")
    log_file = f"{log_dir}/sparqler-{datetime.now().strftime('%d-%m-%Y_%H-%M-%S')}.log"
    logging.basicConfig(level=logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')

    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    logs.addHandler(file_handler)
    logs.addHandler(console_handler)

    return logs


logger = setup_logger()


def format_metadata_prefixes(iri) -> str:

    prefixes = {'https://biomedit.ch/rdf/sphn-schema/sphn#': 'sphn:',
                'https://biomedit.ch/rdf/sphn-ontology/sphn#': 'sphn:', 
                'https://biomedit.ch/rdf/sphn-schema/nexus#': 'nexus:', 
                'http://www.w3.org/2001/XMLSchema#': 'xsd:'
    }

    for key, prefix in prefixes.items():
        if iri.startswith(key):
            iri = iri.replace(key, prefix)
            return iri    


def get_concepts(graph: Graph) -> dict:
    """
    Get concepts from graph. Exclude deprecated concepts.

    :param graph: rdflib Graph Object (Simplified version!)
    :return: dict with concepts as keys and properties dict as values
    """
    concepts_with_prop = {}
    concepts = [c for c in graph.subjects(RDF.type, OWL.Class) if not isinstance(c, BNode)]
    deprecated_concepts = [d for d in (graph.subjects(RDFS.subClassOf, URIRef(DEPRECATED_SPHN_PREFIX))) if
                           isinstance(d, URIRef)]
    deprecated_concepts.append(URIRef(DEPRECATED_SPHN_PREFIX))
    concepts = [concept for concept in concepts if concept not in deprecated_concepts]
    for concept in concepts:
        properties = list(graph.subjects(RDFS.domain, concept))
        concepts_with_prop[concept] = {'properties': properties}
    return concepts_with_prop


def get_properties_and_ranges(graph: Graph) -> tuple:
    """
    Get Data and Object type properties and their respective ranges. 

    :param graph: rdflib Graph Object (Simplified version!)
    :return: tuple of dictionaries with properties and their ranges
                (data prop{prop : {ranges: [range]}}, object_prop{prop: {ranges: [range]}})
    """
    data_properties_and_ranges = {}
    object_properties_and_ranges = {}

    data_properties = set(list(graph.subjects(RDF.type, OWL.DatatypeProperty)))
    object_properties = list(graph.subjects(RDF.type, OWL.ObjectProperty))

    for object_property in object_properties:
        ranges = list(graph.objects(object_property, RDFS.range))
        object_properties_and_ranges[object_property] = {'ranges': ranges}

    for data_property in data_properties:
        ranges = list(graph.objects(data_property, RDFS.range))
        data_properties_and_ranges[data_property] = {'ranges': ranges}

    return data_properties_and_ranges, object_properties_and_ranges


def create_sub_dirs(dir_path: Path, metadata_mode: bool):
    os.makedirs(dir_path, exist_ok=True)
    if metadata_mode:
        sub_dirs = ['has_terminology', 'count_instances']
    else:
        sub_dirs = ['concept_flattening', 'count_instances', 'hasCode', 'min_max_predicates']
    for sub_dir in sub_dirs:
        os.makedirs(os.path.join(dir_path, sub_dir), exist_ok=True)


def dfs_flattening(concepts, data_properties, object_properties, restrictions, concept, prefixes):
    def scan_concept(restricted_concept, base_path, scanned):
        restrictedConceptStr= str(restricted_concept)
        if isinstance(restricted_concept, str):
            restricted_concept = URIRef(restricted_concept)
        concepts_scanned = scanned + [restricted_concept]
        if restricted_concept in concepts and 'properties' in concepts[restricted_concept]:
            for prop in concepts[restricted_concept]['properties']:
                path = base_path + format_prefix(prop, prefixes)
                path_list.append(path.lstrip("/"))  # strip the leading '/'
                concept_prop_path = str(restricted_concept) + str(prop)
                if prop in data_properties:
                    pass
                #if "DataProvider" in str(concept):
                #    logging.INFO("break")
                elif concept_prop_path in restrictions and all(
                        restriction.startswith(tuple(GLOBAL_PREFIXES)) for restriction in
                        restrictions[concept_prop_path]):
                    for nested_concept in restrictions[concept_prop_path]:
                        if URIRef(nested_concept) not in concepts_scanned:
                            scan_concept(nested_concept, path, concepts_scanned)
                else:
                    for nested_concept in object_properties[prop]['ranges']:
                        if nested_concept not in concepts_scanned:
                            scan_concept(nested_concept, path, concepts_scanned)

    path_list = list()
    scan_concept(concept, str(), list())

    return path_list

def dfs_flatteningIntermediateCounts(concepts, data_properties, object_properties, restrictions, concept, prefixes):
    def scan_concept_intermediates(restricted_concept, base_path, scanned):
        restrictedConceptStr= str(restricted_concept)
        if isinstance(restricted_concept, str):
            restricted_concept = URIRef(restricted_concept)
        concepts_scanned = scanned + [restricted_concept]
        if restricted_concept in concepts and 'properties' in concepts[restricted_concept]:
            for prop in concepts[restricted_concept]['properties']:
                path = base_path + format_prefix(prop, prefixes)
                concept_prop_path = str(restricted_concept) + str(prop)
                if prop in data_properties:
                    for elem in data_properties[prop]['ranges']:
                        path_list.append((path.lstrip("/"), str(elem)))
                #if "DataProvider" in str(concept):
                #    logging.INFO("break")
                elif concept_prop_path in restrictions and all(
                        restriction.startswith(tuple(GLOBAL_PREFIXES)) for restriction in
                        restrictions[concept_prop_path]):
                    for nested_concept in restrictions[concept_prop_path]:
                        path_list.append((path.lstrip("/"), str(nested_concept))) 
                        if URIRef(nested_concept) not in concepts_scanned:
                            scan_concept_intermediates(nested_concept, path, concepts_scanned)
                else:
                    for nested_concept in object_properties[prop]['ranges']:
                        path_list.append((path.lstrip("/"), str(nested_concept))) 
                        if nested_concept not in concepts_scanned:
                            scan_concept_intermediates(nested_concept, path, concepts_scanned)

    path_list = list(tuple())
    scan_concept_intermediates(concept, str(), list())

    return path_list


def extract_concept_name(concept):
    concept = str(concept)
    for p in GLOBAL_PREFIXES:
        if concept.startswith(p + '#'):
            if p == SPHN_NAMESPACE:
                return concept.rsplit('#', 1)[1]
            else:
                concept_prefix = p.rsplit('/', 1)[1]
                return f"{concept_prefix}_{concept.split('#', 1)[1]}"


def extract_concept_name_with_prefix(concept, prefixes):
    concept = str(concept)
    for i, j in prefixes.items():
        concept = concept.replace(j, f"{i}:")
    return concept


def render_flattening(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env):
    for concept in concepts:
        paths = dfs_flattening(concepts, data_properties, object_properties, restrictions, concept)
        template = jinja_env.get_template('flattening.rq')
        lines = []
        for path in paths:
            element = path.replace(GLOBAL_PREFIXES[-1], '')
            element = element.replace('sphn:has', '')
            element = element.replace('<', '')
            element = element.replace('>', '')
            element = element.replace('#', '')
            element = element.replace('/', '_')
            lines.append(str(path) + ' ?' + element)
        rendered_content = template.render(Concept=concept, paths=lines)
        file_path = os.path.join(output_dir, 'concept_flattening', extract_concept_name(concept) + '.rq')
        with open(file_path, mode='w', encoding="utf-8") as file:
            file.write(rendered_content)
    return

def render_count(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env,
                 sphn_connector=False, metadata=True, metadata_server='graphdb', metadata_graph=None, prefixes: dict = None, terminology_graph: str=None, root_nodes=None
):
    if sphn_connector:
        template = jinja_env.get_template('count_connector.rq')
        file_path = os.path.join(output_dir, 'count_instances/count-instances.rq')
        file = open(file_path, mode='w', encoding="utf-8")
        file.write(
            f"PREFIX sphn:<{SPHN_NAMESPACE}#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nSELECT *\nWHERE\n{{\n")
        
    else:
        template = jinja_env.get_template('count.rq')
        formatted_object_properties = simplify_object_properties(object_properties, prefixes)
        formatted_restrictions = simplify_object_properties(restrictions, prefixes)
        formatted_data_properties = simplify_object_properties(data_properties, prefixes)
        all_prefixes = '\n'.join([f"PREFIX {key}: <{value}>" for key, value in prefixes.items()])

    for concept_index, concept in enumerate(sorted(concepts.keys()), start=0):
        paths = dfs_flatteningIntermediateCounts(concepts, data_properties, object_properties, restrictions, concept, prefixes)
        lines = [str(path) for (path, node) in paths]
        if sphn_connector:
            concept_prefix, concept_name = niceify_prefix(prefixable_iri=concept)
            concept_str = f"{concept_prefix}:{concept_name}"
            rendered = template.render(Concept=concept, paths=lines, ConceptStr=concept_str)
            if concept_index == 0:
                file.write(f"{{{rendered}}}")
            else:
                file.write(" UNION \n" + f"{{{rendered}}}")
        elif metadata:
            try:
                for index_value, (path, node) in enumerate(paths, start=1):
                    concept_name = extract_concept_name(concept)
                    concept_dir = os.path.join(output_dir, 'count_instances', extract_concept_name(concept))
                    os.makedirs(concept_dir, exist_ok=True)
                    type_query =  check_intermediate_nodes(path=str(path), target=node, concept=concept, object_properties=formatted_object_properties, restrictions=formatted_restrictions, prefixes=prefixes, root_nodes=root_nodes)
                    if metadata_graph:
                        if type_query:
                            for i in type_query:
                                context = create_context_for_jinja(concept=concept, path=str(path), property_path = str(prefixed_path_to_full_iri_path(path, prefixes)), graph=metadata_graph, server_type=metadata_server, type_query=type_query[i])
                                rendered = template.render(context)
                    else:
                        if type_query:
                            length_type_query = len(type_query)
                            for index, type_query in enumerate(type_query, start=1):
                                context = create_context_for_jinja(concept=concept, path=str(path), property_path = str(prefixed_path_to_full_iri_path(path, prefixes)), server_type=metadata_server, type_query=type_query)
                                rendered = template.render(context)
                                if length_type_query > 1:
                                    file_path = os.path.join(concept_dir, f"{extract_concept_name(concept)}-{index_value}-{index}" + '.rq')
                                else:
                                    file_path = os.path.join(concept_dir, f"{extract_concept_name(concept)}-{index_value}" + '.rq')
                                with open(file_path, 'w', encoding="utf-8") as file:
                                    file.write(all_prefixes)
                                    file.write('\n')
                                    file.write(rendered)
                        else:
                            last_path = str(path).split('/')[-1]
                            if last_path in formatted_data_properties:
                                datatype = formatted_data_properties[last_path]['ranges']
                                if len(datatype) > 1:
                                    upper_level = str(path).split('/')[-2]
                                    if upper_level in formatted_data_properties:
                                        range = formatted_data_properties[upper_level]['ranges']
                                    else:    
                                        range = formatted_object_properties[upper_level]['ranges']
                                    if len(range) == 1:
                                        range = str(range[0])
                                        res_path = format_metadata_prefixes(range) + '/' + last_path
                                        if res_path in formatted_restrictions:
                                            datatype = formatted_restrictions[res_path]

                            if isinstance(datatype, list) and len(datatype) == 1:
                                datatype = format_metadata_prefixes(str(datatype[0]))
                            else:
                                datatype = format_metadata_prefixes(str(datatype))

                            context = create_context_for_jinja(sphn_prefix = SPHN_NAMESPACE, concept=concept, path=str(path), property_path = str(prefixed_path_to_full_iri_path(path, prefixes)), server_type=metadata_server, type_query=None, datatype=datatype)
                            rendered = template.render(context)
                            file_path = os.path.join(concept_dir, f"{extract_concept_name(concept)}-{index_value}" + '.rq')
                            with open(file_path, 'w', encoding="utf-8") as file:
                                file.write(all_prefixes)
                                file.write('\n')
                                file.write(rendered)
            except Exception as e:
                logger.error("Failed to generate template for count %s: %s", concept, e)
        else:
            rendered = template.render(Concept=concept, paths=lines)
            file_path = os.path.join(output_dir, 'count_instances', extract_concept_name(concept) + '.rq')
            with open(file_path, 'w', encoding="utf-8") as file:
                file.write(rendered)

    if sphn_connector:
        file.write('\n}')
        file.close()

    return


def prefixed_path_to_full_iri_path(prefixed_path: str, prefixes: dict):
    """
    Creates a full IRI path by expanding the prefixes
    """
    full_iri_path = []
    try:
        for property in prefixed_path.split("/"):
            prefix, prop_name = property.split(":")
            expanded_property = "<" + prefixes[prefix] + prop_name + ">"
            full_iri_path.append(expanded_property)
    except Exception as e:
        logger.error("failed to expand path " + str(prefixed_path))
        logger.error(e)
    return "/".join(full_iri_path)



def render_min_max_connector(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env):
    template_fast = jinja_env.get_template('min_max_fast_connector.rq')
    template_ext = jinja_env.get_template('min_max_extensive_connector.rq')
    file_path_fast = os.path.join(output_dir, 'min_max_predicates/min-max-predicates-fast.rq')
    file_path_ext = os.path.join(output_dir, 'min_max_predicates/min-max-predicates-extensive.rq')
    file_fast = open(file_path_fast, 'w')
    file_ext = open(file_path_ext, 'w')
    prefixes = f"PREFIX sphn:<{SPHN_NAMESPACE}#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nSELECT *\nWHERE\n{{\n"
    file_fast.write(prefixes)
    file_ext.write(prefixes)
    first_fast = True
    first_ext = True
    for concept in sorted(concepts.keys()):
        concept_prefix, concept_name = niceify_prefix(prefixable_iri=concept)
        concept_str = f"{concept_prefix}:{concept_name}"
        paths = dfs_flattening(concepts, data_properties, object_properties, restrictions, concept)
        lines = []
        for path in paths:
            last_path = URIRef(path.split('/')[-1].replace('sphn:', f'{SPHN_NAMESPACE}#'))
            if last_path in data_properties:
                for dType in data_properties[last_path]['ranges']:
                    if dType.split('#')[-1] in (
                            "double", "float", "decimal", "integer", "int", "long", "dateTime", "gYear", "gMonth",
                            "gDay",
                            "time", "date"):
                        lines.append(path)
                    else:
                        continue

            first_fast = write_min_max_queries(file=file_fast, template=template_fast, concept=concept, lines=lines,
                                               concept_str=concept_str, first=first_fast)
            first_ext = write_min_max_queries(file=file_ext, template=template_ext, concept=concept, lines=lines,
                                              concept_str=concept_str, first=first_ext)
        file_fast.write('\n}')
        file_ext.write('\n}')
        file_fast.close()
        file_ext.close()


def render_min_max(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env):
    template = jinja_env.get_template('min_max.rq')
    for concept in sorted(concepts.keys()):
        paths = dfs_flattening(concepts, data_properties, object_properties, restrictions, concept)
        lines = []
        for path in paths:
            last_path = URIRef(path.split('/')[-1].replace('sphn:', f'{SPHN_NAMESPACE}#'))
            if last_path in data_properties:
                for dType in data_properties[last_path]['ranges']:
                    if dType.split('#')[-1] in (
                            "double", "float", "decimal", "integer", "int", "long", "dateTime", "gYear", "gMonth",
                            "gDay",
                            "time", "date"):
                        lines.append(path)
                    else:
                        continue
            rendered = template.render(Concept=concept, paths=lines)
            file_path = os.path.join(output_dir, 'min_max_predicates', extract_concept_name(concept) + '.rq')
            with open(file_path, 'w') as file:
                file.write(rendered)


def render_has_code(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env):
    template = jinja_env.get_template('hasCode.rq')
    for concept in sorted(concepts.keys()):
        paths = dfs_flattening(concepts, data_properties, object_properties, restrictions, concept)
        lines = []
        for path in paths:
            last_path = path.split('/')[-1][5:]
            if last_path.startswith('has') and last_path.endswith('Code'):
                lines.append(path)
            else:
                continue
            rendered = template.render(Concept=concept, paths=lines)
            file_path = os.path.join(output_dir, 'hasCode', extract_concept_name(concept) + '.rq')
            with open(file_path, 'w') as file:
                file.write(rendered)


def render_has_terminology(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env,
                           metadata_server='graphdb', metadata_graph=None, prefixes=None, terminology_graph: str = None):
    
    template = jinja_env.get_template('hasTerminology.rq')
    all_prefixes = '\n'.join([f"PREFIX {key}: <{value}>" for key, value in prefixes.items()])
    for concept in sorted(concepts.keys()):
        try:
            paths = dfs_flattening(concepts, data_properties, object_properties, restrictions, concept, prefixes)
            lines = []
            path_ind = 1
            for path in paths:
                last_path = path.split('/')[-1][5:]
                if last_path.startswith('has') and last_path.endswith('Code'):
                    lines.append(path)
                else:
                    continue
                if metadata_graph:
                    rendered = template.render(Concept=concept, path=path, graph=metadata_graph, server_type=metadata_server, sphn_prefix = SPHN_NAMESPACE)
                else:
                    rendered = template.render(Concept=concept, path=path, graph=None, server_type=metadata_server, sphn_prefix = SPHN_NAMESPACE)
                concept_dir = os.path.join(output_dir, 'has_terminology', extract_concept_name(concept))
                os.makedirs(concept_dir, exist_ok=True)
                file_path = os.path.join(concept_dir, f"{extract_concept_name(concept)}-{path_ind}" + '.rq')
                with open(file_path, 'w') as file:
                    file.write(all_prefixes)
                    file.write('\n')
                    file.write(rendered)
                path_ind += 1
        except Exception as e:
            logger.error(f"Failed to generate template for has terminology {concept}: {e}")

def render_list_prefix(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env,
                           metadata_server='graphdb', metadata_graph=None, prefixes=None, terminology_graph: str = None):
    
    template = jinja_env.get_template('list_prefix.rq')
    all_prefixes = '\n'.join([f"PREFIX {key}: <{value}>" for key, value in prefixes.items()])
    root_prefixes_to_remove = []
    for pref, iri in prefixes.items():
        if iri[:-1] in GLOBAL_PREFIXES or iri in GLOBAL_PREFIXES:
            root_prefixes_to_remove.append(pref)

    prefixes_to_scan = list(set(prefixes.keys()) - set(TERMINOLOGY_PREFIX_EXCLUSIONS) - set(root_prefixes_to_remove))

    for prefix in prefixes_to_scan:
        try:
                if metadata_graph:
                    rendered = template.render(prefix=prefix, graph=metadata_graph, server_type=metadata_server)
                else:
                    rendered = template.render(prefix=prefix, graph=None, server_type=metadata_server)
                prefix_dir = os.path.join(output_dir, 'list_prefix', prefix)
                os.makedirs(prefix_dir, exist_ok=True)
                file_path = os.path.join(prefix_dir, f"{prefix}" + '.rq')
                with open(file_path, 'w') as file:
                    file.write(all_prefixes)
                    file.write('\n')
                    file.write(rendered)
        except Exception as e:
            logger.error(f"Failed to generate template for list_prefix {prefix}: {e}")

def render_total_count(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env,
                           metadata_server='graphdb', metadata_graph=None, prefixes=None, terminology_graph: str = None):
    
    template = jinja_env.get_template('total_count.rq')
    all_prefixes = '\n'.join([f"PREFIX {key}: <{value}>" for key, value in prefixes.items()])
    try:
            if metadata_graph:
                rendered = template.render(graph=metadata_graph, server_type=metadata_server)
            else:
                rendered = template.render(graph=None, server_type=metadata_server)
            prefix_dir = os.path.join(output_dir, 'total_count', 'total_count')
            os.makedirs(prefix_dir, exist_ok=True)
            file_path = os.path.join(prefix_dir, f"total_count" + '.rq')
            with open(file_path, 'w') as file:
                file.write(all_prefixes)
                file.write('\n')
                file.write(rendered)
    except Exception as e:
        logger.error(f"Failed to generate template for total_count: {e}")


def render_has_code_connector(concepts, data_properties, object_properties, restrictions, output_dir, jinja_env):
    template = jinja_env.get_template('hasCode_connector.rq')
    file_path = os.path.join(output_dir, 'hasCode/has-code.rq')
    file = open(file_path, 'w')
    file.write(
        f"PREFIX sphn:<{SPHN_NAMESPACE}#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nSELECT *\nWHERE\n{{\n")
    first = True
    for concept in sorted(concepts.keys()):
        concept_prefix, concept_name = niceify_prefix(prefixable_iri=concept)
        concept_str = f"{concept_prefix}:{concept_name}"
        paths = dfs_flattening(concepts, data_properties, object_properties, restrictions, concept)
        lines = []
        for path in paths:
            last_path = path.split('/')[-1][5:]
            if last_path.startswith('has') and last_path.endswith('Code'):
                lines.append(path)
            else:
                continue
        rendered = template.render(Concept=concept, paths=sorted(lines), ConceptStr=concept_str)
        if 'SELECT ?concept ?origin' not in rendered:
            continue
        if first:
            file.write(f"{{{rendered}}}")
            first = False
        else:
            file.write(" UNION \n" + f"{{{rendered}}}")
        file.write('\n}')
        file.close()


def write_min_max_queries(file: typing.IO, template: Template, concept: str, lines: list, concept_str: str,
                          first: bool):
    """Write Connector queries
    """
    rendered = template.render(Concept=concept, paths=sorted(lines), ConceptStr=concept_str)
    if 'SELECT ?concept ?origin' not in rendered:
        return first
    if first:
        file.write(f"{{{rendered}}}")
        first = False
    else:
        file.write(" UNION \n" + f"{rendered}")
    return first


def add_global_prefixes(root_nodes):
    for root_node in root_nodes:
        if (root_node.startswith('https://biomedit.ch/rdf/sphn-schema/') or root_node.startswith('https://biomedit.ch/rdf/sphn-ontology/')) and '#' in root_node:
            prefix = root_node.split('#')[0]
            if prefix not in GLOBAL_PREFIXES:
                GLOBAL_PREFIXES.append(prefix)


def run_sparqler(sphn_schema: str,
                 output_dir: str = DEFAULT_OUTPUT_DIR,
                 connector: bool = False,
                 metadata: bool = False,
                 metadata_server_type: str = 'graphdb',
                 metadata_graph: str = None,
                 project_schema: str = None,
                 terminology_graph: str = None,
                 generate_sparqls: bool = False
                 ):
    
    sphn_graph = load_graph(Path(sphn_schema), 'turtle')
    if project_schema:
        project_graph = load_graph(Path(project_schema), 'turtle')
    else:
        project_graph = Graph()
    root_nodes, simplified_graph = simplify_schema_graph(sphn_graph=sphn_graph, project_graph=project_graph)
    prefixes = dict(simplified_graph.namespaces())
    for p, iri in prefixes.items():
        prefixes[p] = str(iri)

    add_global_prefixes(root_nodes)
    global SPHN_NAMESPACE
    if SPHN in GLOBAL_PREFIXES:
        SPHN_NAMESPACE = SPHN
    else:
        SPHN_NAMESPACE = ONTOLOGY
    
    prefixes_to_keep = {
                        'xsd': prefixes['xsd']
                        }
    for gp in GLOBAL_PREFIXES:
        if gp ==  SPHN_NAMESPACE:
            prefixes_to_keep.update({'sphn': gp + '#'})
        else:
            project_prefix = [p for p, i in prefixes.items() if  i == gp + '#'][0]
            prefixes_to_keep.update({project_prefix: gp + '#'})
    terminology_prefixes = dict(set(prefixes.items()) - set(prefixes_to_keep.items()))
    concepts = get_concepts(simplified_graph)
    data_properties, object_properties = get_properties_and_ranges(simplified_graph)
    restrictions = get_restrictions_map(get_restrictions(simplified_graph, list(concepts.keys())))
    if project_schema:
        if os.path.sep in project_schema:
            project_name = project_schema.replace('\\', '/').rsplit('/', 1)[1].rsplit('.', 1)[0]
        else:
            project_name = project_schema.rsplit(".", 1)[0]
    else:
        if os.path.sep in sphn_schema:
            project_name = sphn_schema.replace('\\', '/').rsplit('/', 1)[1].rsplit('.', 1)[0]
        else:
            project_name = sphn_schema.rsplit(".", 1)[0]
    output_dir = f"{output_dir}/{project_name}"
    if not generate_sparqls:
        return terminology_prefixes, prefixes_to_keep
    else :
        create_sub_dirs(Path(output_dir), metadata_mode=metadata)
        jinja_env = Environment(loader=FileSystemLoader(os.path.join(SCRIPT_DIR, 'templates')))

        if connector:
            render_count(concepts=concepts,
                        data_properties=data_properties,
                        object_properties=object_properties,
                        restrictions=restrictions,
                        output_dir=output_dir,
                        sphn_connector=connector,
                        jinja_env=jinja_env,
                        root_nodes=root_nodes
                        )

            render_min_max_connector(concepts=concepts,
                                    data_properties=data_properties,
                                    object_properties=object_properties,
                                    restrictions=restrictions,
                                    output_dir=output_dir,
                                    jinja_env=jinja_env
                                    )

            del concepts[URIRef(f"{SPHN_NAMESPACE}#Code")]
            render_has_code_connector(concepts=concepts,
                                    data_properties=data_properties,
                                    object_properties=object_properties,
                                    restrictions=restrictions,
                                    output_dir=output_dir,
                                    jinja_env=jinja_env
                                    )
        elif metadata:
            render_count(concepts=concepts,
                        data_properties=data_properties,
                        object_properties=object_properties,
                        restrictions=restrictions,
                        output_dir=output_dir,
                        sphn_connector=connector,
                        jinja_env=jinja_env,
                        metadata=True,
                        metadata_server=metadata_server_type,
                        metadata_graph=metadata_graph,
                        terminology_graph=terminology_graph,
                        prefixes=prefixes,
                        root_nodes=root_nodes
                        )

            del concepts[URIRef(f"{SPHN_NAMESPACE}#Code")]
            render_has_terminology(concepts=concepts,
                                data_properties=data_properties,
                                object_properties=object_properties,
                                restrictions=restrictions,
                                output_dir=output_dir,
                                jinja_env=jinja_env,
                                metadata_server=metadata_server_type,
                                metadata_graph=metadata_graph,
                                terminology_graph=terminology_graph,
                                prefixes=prefixes
                                )
            
            render_list_prefix(concepts=concepts,
                                data_properties=data_properties,
                                object_properties=object_properties,
                                restrictions=restrictions,
                                output_dir=output_dir,
                                jinja_env=jinja_env,
                                metadata_server=metadata_server_type,
                                metadata_graph=metadata_graph,
                                terminology_graph=terminology_graph,
                                prefixes=prefixes
                                )
            render_total_count(concepts=concepts,
                                data_properties=data_properties,
                                object_properties=object_properties,
                                restrictions=restrictions,
                                output_dir=output_dir,
                                jinja_env=jinja_env,
                                metadata_server=metadata_server_type,
                                metadata_graph=metadata_graph,
                                terminology_graph=terminology_graph,
                                prefixes=prefixes
                                )
        else:
            render_flattening(concepts=concepts,
                            data_properties=data_properties,
                            object_properties=object_properties,
                            restrictions=restrictions,
                            output_dir=output_dir,
                            jinja_env=jinja_env,
                            root_nodes=root_nodes
                            )

            render_count(concepts=concepts,
                        data_properties=data_properties,
                        object_properties=object_properties,
                        restrictions=restrictions,
                        output_dir=output_dir,
                        sphn_connector=connector,
                        jinja_env=jinja_env
                        )

            render_min_max(concepts=concepts,
                        data_properties=data_properties,
                        object_properties=object_properties,
                        restrictions=restrictions,
                        output_dir=output_dir,
                        jinja_env=jinja_env
                        )

            del concepts[URIRef(f"{SPHN_NAMESPACE}#Code")]
            render_has_code(concepts=concepts,
                            data_properties=data_properties,
                            object_properties=object_properties,
                            restrictions=restrictions,
                            output_dir=output_dir,
                            jinja_env=jinja_env
                            )
        
        return terminology_prefixes, prefixes_to_keep


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Script to generate SPARQL queries")

    parser.add_argument('--sphn-schema', dest="sphn_schema", required=True,
                        default=None, help="Path to sphn schema (required)", type=str)

    parser.add_argument('--project-schema', dest="project_schema", default=None,
                        help="Path to project schema (required)", type=str)

    parser.add_argument('--named-graph', dest="named_graph", default=None,
                        help="Named graph to query", type=str)

    parser.add_argument('--terminology-graph', dest="terminology_graph", default=None,
                        help="Terminology Graph", type=str)

    parser.add_argument('--output-dir', dest='output_dir', default='./sparql-queries',
                        help="Output directory for sparql queries (required)", type=str)

    parser.add_argument('--connector', dest='connector', default=False,
                        help="Run Connector version", type=bool)

    parser.add_argument('--metadata', dest='metadata', default=False,
                        help="Run Metadata version", type=bool)

    parser.add_argument('--sparqls', dest='sparqls', default='True',
                        help="Directory of sparql templates to run", type=str)

    args = parser.parse_args()

    run_sparqler(sphn_schema=args.sphn_schema,
                 project_schema=args.project_schema,
                 output_dir=args.output_dir,
                 connector=args.connector,
                 metadata=True)
