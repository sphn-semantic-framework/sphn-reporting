"""
Metadata generator script

Created: May 2024
"""
import os
import sys
import logging
import argparse
import shutil
from time import time, sleep
from datetime import datetime, timedelta
import pandas as pd
import json 
import re
import requests
import concurrent.futures

from urllib.error import URLError
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON, BASIC

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'sparqler'))
from sparqler.sparqler import run_sparqler
from sparqler.rml_generator_lib import load_graph

import sqlite_converter 

# suppress insecure request warning to einstein
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Default values
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DEFAULT_SPARQLS_ROOT_DIR = os.path.join(SCRIPT_DIR, 'sparqls')
DEFAULT_METADATA_ROOT_DIR = os.path.join(SCRIPT_DIR, 'metadata')
DEFAULT_GEN_SPARQLS = 1
DEFAULT_DB = 'graphdb'
DEFAULT_SPARQLS_SUB_DIR = ['has_terminology', 'count_instances']
RETRY_ATTEMPTS = 3
RETRY_DELAY = 2 # seconds
RENEW_EINSTEIN_TOKEN_TIME = 5 # min
EINSTEIN_TOKEN_CREATION_TIME = 0
SUCCESS_QUERIES = 0
FAILED_QUERIES = 0

def setup_logger() -> logging.Logger:
    """
    Setup logger for the Metadata Generator
    :return: logger object
    """
    log_dir = os.path.join(SCRIPT_DIR, 'logs')
    os.makedirs(log_dir, exist_ok=True)
    logs = logging.getLogger("MetadataGenerator")
    log_file = f"{log_dir}/metadata-generator-{datetime.now().strftime('%d-%m-%Y_%H-%M-%S')}.log"
    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')

    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    console_handler.setLevel(logging.DEBUG)

    logs.addHandler(file_handler)
    logs.addHandler(console_handler)
    logs.propagate = False

    return logs



def extract_project_name(project_schema: str, sphn_schema: str) -> str:
    """
    Extracts the project name from the project/SPHN schema
    :param project_schema: Path of project schema
    :param sphn_schema: Path of SPHN schema
    :return: extracted project name
    """
    project_name = None
    if project_schema:
        logger.info(f"Running project schema mode: {project_schema}")
        if os.path.sep in project_schema:
            project_name = project_schema.replace('\\', '/').rsplit('/', 1)[1].rsplit('.', 1)[0]
        else:
            project_name = project_schema.rsplit(".", 1)[0]
    else:
        logger.info(f"Running SPHN schema mode: {sphn_schema}")
        if os.path.sep in sphn_schema:
            project_name = sphn_schema.replace('\\', '/').rsplit('/', 1)[1].rsplit('.', 1)[0]
        else:
            project_name = sphn_schema.rsplit(".", 1)[0]
    return project_name


def sanitize(s: str) -> str:
    """
    Sanitize string by replacing unusual/unexpected characters. Adapted from dataset2rdf.
    :param s: The string to be sanitized
    :return: The sanitized string
    """
    # replace 'Zero width space' (0x200B)
    s = str(s).replace("​", " ")
    # replace 'Zero width space' (\xa0)
    s = s.replace(" ", " ")
    # replace newline chars with spaces
    s = s.replace("\n", " ")
    # replace leading and trailing spaces
    s = s.strip()
    return s


def prefix_column_if_exists(df:pd.DataFrame, column_name:str, terminology_prefixes:dict, replace_http:bool):
    """
    applies prefixing on a column
    """
    for ind, row in df.iterrows():
        row_str = list(row)
        df_cols = list(df.columns)
        column_val = ''
        if column_name in row:
            column_val = row[column_name]
            if str(column_val) == "nan":
                column_val = ''
        if replace_http:
            if ':' in column_val:
                for k, v in terminology_prefixes.items():
                    if column_val.startswith(v):
                        df.iloc[ind, df.columns.get_loc(column_name)] = k
        else:
            if ':' in column_val and not column_val.startswith("http://") and not column_val.startswith("https://"):
                for k, v in terminology_prefixes.items():
                    if column_val.startswith(v):
                        df.iloc[ind, column_name] = k
    return df


def parse_response(response: dict, terminology_prefixes: dict, sparql_sub_dir: str) -> pd.DataFrame:
    """
    Parses the response received from the SPARQL endpoint into a Pandas DataFrame
    :param response: The serialized result of the query
    :return: The result parsed as a Pandas DataFrame
    """
    result = pd.json_normalize(response["results"]["bindings"])
    if (sparql_sub_dir == "has_terminology" and len (result.index) > 0):
        # replace the codes before summarizing to ensure proper summarization
        prefix_column_if_exists(result, "target_concept.value", terminology_prefixes, False)
        prefix_column_if_exists(result, "origin.value", terminology_prefixes, True)
        prefix_column_if_exists(result, "code.value", terminology_prefixes, True)
        result = summarize_terminology_listing(result, terminology_prefixes)
    elif (sparql_sub_dir == "list_prefix" and len(result.index) > 0):
        result = summarize_prefix_listing(result)
        prefix_column_if_exists(result, "target_concept.value", terminology_prefixes, False)
        prefix_column_if_exists(result, "origin.value", terminology_prefixes, True)
        prefix_column_if_exists(result, "code.value", terminology_prefixes, True)
    else :
        prefix_column_if_exists(result, "target_concept.value", terminology_prefixes, False)
        prefix_column_if_exists(result, "origin.value", terminology_prefixes, True)
        prefix_column_if_exists(result, "code.value", terminology_prefixes, True)
    return result


def summarize_terminology_listing(result: pd.DataFrame, terminology_prefixes: dict):
    """
    Groups the results by ?code (ext. terminology) and ?code_codingSystemAndVersion (codes)
    """
    if set(['origin.value', 'path.value', 'code.value', 'code_codingSystemAndVersion.value', 'count_instances.value']).issubset(result.columns):
        summarized_terminology_listing = result[['origin.value', 'path.value', 'code.value', 'code_codingSystemAndVersion.value', 'count_instances.value']].groupby(by=['origin.value', 'path.value', 'code.value', 'code_codingSystemAndVersion.value'], dropna=True, as_index=False, sort=False).count()
    elif set(['origin.value', 'path.value', 'code.value', 'count_instances.value']).issubset(result.columns):
        summarized_terminology_listing = result[['origin.value', 'path.value', 'code.value','count_instances.value']].groupby(by=['origin.value', 'path.value', 'code.value'], dropna=True, as_index=False, sort=False).count()
    elif set(['origin.value', 'path.value', 'code_codingSystemAndVersion.value', 'count_instances.value']).issubset(result.columns):
        summarized_terminology_listing = result[['origin.value', 'path.value', 'code_codingSystemAndVersion.value', 'count_instances.value']].groupby(by=['origin.value', 'path.value', 'code_codingSystemAndVersion.value'], dropna=True, as_index=False, sort=False).count()
    else:
        summarized_terminology_listing = result
        summarized_terminology_listing['count_instances.value'] = 0
    return summarized_terminology_listing


def summarize_prefix_listing(result: pd.DataFrame):
    """
    Reduces a prefix listing to the count and distinct count of code values.
    :param result: dataframe containing two columns: code.type,code.value with a lot of rows.
    """
    distinct_count = len(pd.unique(result['code.value']))
    count = len((result['code.value']))

    new_data = {'code.type': ['uri', 'uri'],
                'code.value' : [result['code.value'][0], result['code.value'][0]],
                'count_instances.value' : [count, distinct_count],
                'isDistinct' : [False, True] }

    return pd.DataFrame(new_data)



def query_wrapper(sparql_endpoint: SPARQLWrapper, query: str) -> dict:
    """
    Executes the SPARQL query against a given SPARQLWrapper endpoint
    :param sparql_endpoint: The SPARQLWrapper endpoint to query
    :param query: The SPARQL query to execute
    :return: Query result as a dictionary
    """
    global SUCCESS_QUERIES
    global FAILED_QUERIES
    for attempt in range(1, RETRY_ATTEMPTS + 1):
        try:
            sparql_endpoint.setQuery(query)
            results = sparql_endpoint.query().convert()
            SUCCESS_QUERIES += 1
            return results
        except Exception as e:
            logger.error(e)
            logger.error(f"Failed to execute query. Retrying.. ({attempt}/{RETRY_ATTEMPTS})")
            if attempt < RETRY_ATTEMPTS:
                sleep(RETRY_DELAY)
            else:
                logger.error(f"Max attempts reached. Query failed.")
                logger.error(f"\n{query}")
                FAILED_QUERIES += 1


def get_db_connection(db_host: str,
                      username: str,
                      password: str,
                      data_source: str,
                      server_type: str,
                      timeout: int = 1800,
                      ) -> SPARQLWrapper:
    """
    Prepares a SPARQLWrapper endpoint for connecting to the database
    :param db_host: The database host & port
    :param username: The database username
    :param password: The database password
    :param data_source: The repository (graphdb) or dataset (fuseki) to query
    :param server_type: The type of database to query (graphdb or fuseki)
    :param timeout: The timeout (in seconds) to apply for SPARQL queries. Default is `1800`
    :return: The SPARQLWrapper object
    """
    if server_type == 'fuseki':
        connection_string = f"http://{db_host}/{data_source}/sparql"
    elif server_type == 'graphdb':
        connection_string = f"http://{db_host}/repositories/{data_source}"
    else:
        connection_string = f"http://{db_host}/virtuoso/sparql"
    connection = SPARQLWrapper(connection_string)
    connection.setCredentials(user=username, passwd=password)
    connection.setHTTPAuth(BASIC)
    connection.setReturnFormat(JSON)
    connection.setTimeout(timeout)
    return connection


def get_einstein_api_token(einstein_ip: str, username: str, password: str) -> str:
    """ 
    Requests an API token from einstein
    :param einstein_ip: Einstein's API host and port ex: localhost:1543
    :param username: Einstein's API username
    :param password: Einstein's API password
    :return: token string
    """
    try:
        response = requests.post(f'https://{einstein_ip}/token', data={'grant-type': 'password', 'username': username, 'password': password}, verify=False)
    
        if response.status_code == 200:
            token = response.json()["access_token"]
            logger.info("Einstein API token generated")
            global EINSTEIN_TOKEN_CREATION_TIME
            EINSTEIN_TOKEN_CREATION_TIME = datetime.now()
            return token
        else:
            raise ConnectionError("Failed to retrieve Einstein API Token")
    except Exception as e:
        logger.error("Failed to connect to Einstein API token")
        raise ConnectionError(e)


def query_einstein(einstein_ip:str, token:str, query: str, username:str =None, password:str=None):
    """
    Query Einstein's sparql endpoint (virtuoso)
    :param einstein_ip: Einstein's API host and port ex: localhost:1543
    :param token: Einstein's API token
    :param query: The Sparql query 
    """
    global SUCCESS_QUERIES
    global FAILED_QUERIES
    for attempt in range(1, RETRY_ATTEMPTS + 1):
        try:
            if int(round((datetime.now() - EINSTEIN_TOKEN_CREATION_TIME).total_seconds() / 60)) > RENEW_EINSTEIN_TOKEN_TIME:
                token = get_einstein_api_token(einstein_ip=einstein_ip, username=username, password=password)
            endpoint = f'https://{einstein_ip}/sparql'
            headers = {"Authorization": f"Bearer {token}", "Accept": "application/sparql-results+json" }
            response = requests.get(endpoint, headers=headers, params={'query': query}, verify=False)
            response.raise_for_status()
            return response
        except Exception as e:
            logger.warning(f"Failed to execute query in einstein. Retrying.. ({attempt}/{RETRY_ATTEMPTS})")
            if attempt < RETRY_ATTEMPTS:
                sleep(RETRY_DELAY)
            else:
                logger.error(f"Max attempts reached. Query failed.")
                logger.error(f"\n{query}")
                FAILED_QUERIES += 1


def test_sparql_endpoint(sparql_endpoint: SPARQLWrapper = None, einstein_token:str = None, einstein_ip:str=None):
    """
    Runs a simple test to check if the SPARQLWrapper is working
    :param sparql_endpoint: The SPARQL endpoint to test
    :return: True if successful, False otherwise
    """
    logger.info(f"Testing SPARQL endpoint")
    query = """
            SELECT (COUNT(*) as ?cnt) where { 
                    ?s ?p ?o
            }"""
    try:
        if sparql_endpoint:
            sparql_endpoint.setQuery(query)
            response = sparql_endpoint.query().convert()
            cnt = response['results']['bindings'][0]['cnt']['value']
        elif einstein_token:
            response = query_einstein(einstein_ip=einstein_ip, token=einstein_token, query=query)
            response= response.json()
        if cnt is not None and int(cnt) > 1:
            logger.info(f"Connection to Database Successful. {cnt} triples found.")
        else:
            logger.warning(f"No data found in Database")
    except Exception as error:
        logger.error(f"Error Testing Database connection: {error}")
        raise Exception(error)


def test_patient_data(sparql_endpoint: SPARQLWrapper = None, einstein_token:str = None, einstein_ip:str=None, sphn_prefix=None):
    """
    Runs a simple test to check if there is patient data in graph
    :param sparql_endpoint: The SPARQL endpoint to test
    :return: True if successful, False otherwise
    """
    logger.info(f"Checking for Patient data in graph")
    query = f"""
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX sphn: <{sphn_prefix}>

                ASK where {{ 
                    {{
                        ?s rdf:type sphn:SubjectPseudoIdentifier
                    }}
                }}"""
    try:
        if sparql_endpoint:
            sparql_endpoint.setQuery(query)
            response = sparql_endpoint.query().convert()
        elif einstein_token:
            response = query_einstein(einstein_ip=einstein_ip, token=einstein_token, query=query)
            response= response.json()
        if response.get('boolean'):
            logger.info(f"Patient data found in graph")
        else:
            logger.warning("Unable to find patient data in graph. Queries might be affected")
    except Exception as error:
        logger.debug(f"Failed to check for Terminology data in graph: {error}")


def test_terminology_data(sparql_endpoint: SPARQLWrapper = None, einstein_token:str = None, einstein_ip:str=None, sphn_prefix=None) -> bool:
    """
    Runs a simple test to check if the the terminology has been loaded
    :param sparql_endpoint: SPARQLWrapper configured for fuseki
    :return: True if successful, False otherwise
    """
    logger.info(f"Checking for Terminology data in graph")
    try:
        if sparql_endpoint:
            query = f"""
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    PREFIX sphn: <{sphn_prefix}>
                    
                    ASK WHERE {{
                        SELECT ?name (COUNT(?s) AS ?numconcepts) WHERE {{
                        {{ ?s rdfs:subClassOf+ sphn:Terminology. 
                            BIND("count elements below Terminology" as ?name)
                        }}
                        
                    }}
                    GROUP BY ?name 
                    HAVING (?numconcepts > 200000 )
                    }} """
            sparql_endpoint.setQuery(query)
            response = sparql_endpoint.query().convert()

        if einstein_token:
            query = f"""
            ASK WHERE {{
                SELECT ?g (COUNT(*) as ?numtriples)
                WHERE {{
                        GRAPH <https://biomedit.ch/rdf/sphn-resource/global> {{
                                    ?s ?p ?o.
                                    BIND(<https://biomedit.ch/rdf/sphn-resource/global> as ?g)
                                }}
                        }}
                        GROUP BY ?g
                        HAVING(COUNT(*) > 10000000) 
                }} 
            """
            response = query_einstein(einstein_ip=einstein_ip, token=einstein_token, query=query).json()

        if response.get('boolean'):
            logger.info(f"Terminology data found in graph")
        else:
            logger.warning("Unable to find terminology data in graph. Queries might be affected")

    except Exception as error:
        logger.debug(f"Failed to check for Terminology data in graph: {error}")
        

def create_output_dirs(project_name: str):
    """
    Creates the directories where output will be stored
    :param project_name: The name of the project
    :return:
    """
    sparqls_dir = os.path.join(DEFAULT_SPARQLS_ROOT_DIR, project_name)
    metadata_dir = os.path.join(DEFAULT_METADATA_ROOT_DIR, project_name)

    # Create the metadata directories. Sparqls directories are created by the SPARQLer
    os.makedirs(DEFAULT_METADATA_ROOT_DIR, exist_ok=True)
    # remove the contents of the project first 
    shutil.rmtree(metadata_dir, ignore_errors=True)
    os.makedirs(metadata_dir, exist_ok=True)
    os.makedirs(os.path.join(str(metadata_dir), "count_instances"), exist_ok=True)
    os.makedirs(os.path.join(str(metadata_dir), "has_terminology"), exist_ok=True)

    return sparqls_dir, metadata_dir


def save_results(results: pd.DataFrame, save_dir: str):
    """
    Saves results to Metadata Directory, in JSON and CSV formats
    :param results: The parsed results from the queried SPARQL endpoint
    :param save_dir: The directory to save the results
    """
    csv_dir = save_dir.rsplit('/', 1)[0] + "/CSV"
    json_dir = save_dir.rsplit('/', 1)[0] + "/JSON"
    file_name = save_dir.rsplit('/', 1)[1]
    os.makedirs(csv_dir, exist_ok=True)
    os.makedirs(json_dir, exist_ok=True)
    results.to_json(f"{json_dir}/{file_name.replace('rq', 'json')}", orient='records', indent=4)
    results.to_csv(f"{csv_dir}/{file_name.replace('rq', 'csv')}", index=False)


def is_sparqls_ready(sparqls_dir: str) -> bool:
    """
    Checks if the SPARQLS directory has been created properly
    :param sparqls_dir: The SPARQLS directory
    :return: True if SPARQLS directory is as expected, False otherwise
    """
    try:
        assert os.path.exists(sparqls_dir)
        for sub_dir in DEFAULT_SPARQLS_SUB_DIR:
            assert sub_dir in os.listdir(sparqls_dir), f"{sub_dir} is not in {sparqls_dir}"
            assert len(os.listdir(os.path.join(sparqls_dir, sub_dir))) > 0
        logger.info("SPARQL queries ready for metadata generation")
        return True
    except AssertionError as error:
        logger.error(error)
        return False
    
def process_query(sparqls_dir, sparql_sub_dir, concept_dir, sparql, query_count, queries_length, sparql_endpoint, einstein_ip, einstein_token, terminology_prefixes, metadata_dir, username, password):
    with open(f"{sparqls_dir}/{sparql_sub_dir}/{concept_dir}/{sparql}", 'r') as file:
        query = file.read()
        logger.info(f"Query {sparql_sub_dir} - {concept_dir} - {query_count}/{queries_length}")
        if sparql_endpoint:
            response = query_wrapper(sparql_endpoint, query)
        if einstein_ip:
            response = query_einstein(einstein_ip=einstein_ip, token=einstein_token, query=query, username=username, password=password).json()
        if response:
            results = parse_response(response, terminology_prefixes, sparql_sub_dir)
            if not results.empty:
                save_results(results, f"{metadata_dir}/{sparql_sub_dir}/{concept_dir}/{sparql}")
    return


def generate_metadata(sparqls_dir: str, metadata_dir: str, sparql_endpoint: SPARQLWrapper = None, einstein_ip:str = None, einstein_token:str = None, terminology_prefixes:dict = None, username:str = None, password:str =None, workers:int = 1):
    """
    Generates the metadata
    :param sparqls_dir: Directory with SPARQLS
    :param metadata_dir: Directory where the metadata will be stored
    :param sparql_endpoint: The SPARQL endpoint to query
    """
    logger.info("Starting to process sparql queries")
    query_count_total = 0
    with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
        futures = []
        for sparql_sub_dir in os.listdir(sparqls_dir):
            for concept_dir in os.listdir(os.path.join(sparqls_dir, sparql_sub_dir)):
                queries_length = len(os.listdir(f"{sparqls_dir}/{sparql_sub_dir}/{concept_dir}"))
                query_count = 0
                for sparql in os.listdir(f"{sparqls_dir}/{sparql_sub_dir}/{concept_dir}"):
                    query_count += 1
                    query_count_total += 1
                    futures.append(executor.submit(process_query, sparqls_dir, sparql_sub_dir, concept_dir, sparql, query_count, queries_length,
                                                   sparql_endpoint, einstein_ip, einstein_token, terminology_prefixes, metadata_dir, username, password))
        for future in concurrent.futures.as_completed(futures):
            try: 
                future.result()
            except Exception as e:
                logger.error(f"A Query failed: {e}")
    logger.info(f"Query execution completed. {query_count_total} queries have been processed.")
    logger.info(f"Queries Passed: {SUCCESS_QUERIES}")
    logger.info(f"Queries Failed: {FAILED_QUERIES}")


def combine_files(metadata_dir: str):
    """
    Combines the split concept metadata files into one
    :param metadata_dir: The directory where the metadata was generated
    """
    logger.info("Processing metadata for export")
    with concurrent.futures.ProcessPoolExecutor() as executor:
        futures = []
        for metadata_type_dir in os.listdir(metadata_dir):
            for concept_metadata in os.listdir(os.path.join(metadata_dir, metadata_type_dir)):
                for metadata_format in os.listdir(os.path.join(metadata_dir, metadata_type_dir, concept_metadata)):
                    if metadata_format == 'CSV':
                        futures.append(executor.submit(combine_csv_files,  os.path.join(metadata_dir, metadata_type_dir, concept_metadata, metadata_format), f"{concept_metadata}.csv"))        
                    if metadata_format == 'JSON':
                        futures.append(executor.submit(combine_json_files,  os.path.join(metadata_dir, metadata_type_dir, concept_metadata, metadata_format), f"{concept_metadata}.json"))        
        for future in concurrent.futures.as_completed(futures):
            try: 
                future.result()
            except Exception as e:
                logger.error(f"A task failed: {e}")


def combine_csv_files(dir: str, filename: str):
    """
    Combines multiple CSV files into one
    :param dir: Directory with CSVs
    :param filename: Name if the concatenated file
    """
    csv_files = [f for f in os.listdir(dir) if f.endswith('.csv')]
    combined_df = pd.concat([pd.read_csv(os.path.join(dir, f)) for f in csv_files])
    combined_df = combined_df.drop_duplicates(keep='first')
    combined_df.to_csv(os.path.join(dir, filename), index=None)
    pattern = re.compile(r'-(\d+)\.csv$')
    for csv_file in csv_files:
        if pattern.search(csv_file):
            os.remove(os.path.join(os.path.join(dir, csv_file)))


def combine_json_files(dir: str, filename: str):
    """
    Combines multiple JSON files into one
    :param dir: Directory with JSON files
    :param filename: Name if the concatenated file
    """
    separatorString = os.sep if os.sep=="/" else "\\"
    json_files = [f for f in os.listdir(dir) if f.endswith('.json')]
    combined_data = []
    for ind, f in enumerate(json_files):
        with open(os.path.join(dir, f), 'r') as file:
            if dir.rsplit(separatorString, 3)[-3] == 'count_instances':
                if ind == 0:
                    combined_data.extend(json.load(file))
                else:
                    combined_data.extend(json.load(file)[1:])
            else:
                    combined_data.extend(json.load(file))

    with open(os.path.join(dir, filename), 'w') as file:
        json.dump(combined_data, file, indent=4)
    pattern = re.compile(r'-(\d+)\.json$')
    for json_file in json_files:
        if pattern.search(json_file):
            os.remove(os.path.join(os.path.join(dir, json_file)))
    

def zip_output(project_name: str, metadata_dir: str, output_dir: str):
    """
    Zip the metadata output
    :param project_name: The name of the project
    :param metadata_dir: The directory where the metadata was generated
    :param output_dir: The directory where the zip will be stored
    """
    logger.info(
        f"Zipping metadata {project_name}-{datetime.now().strftime('%d-%m-%Y_%H-%M-%S')}.zip to {output_dir}")
    shutil.make_archive(f'{output_dir}/{project_name}-{datetime.now().strftime("%d-%m-%Y_%H-%M-%S")}',
                        'zip', metadata_dir)

def delete_sparqls_metadata_dirs():
    """
    Deletes metadata and sparql directories when error encountered or after the metadata has been zipped
    """
    try:
        shutil.rmtree(DEFAULT_METADATA_ROOT_DIR)
    except Exception as error:
        logger.error(f"Failed to delete {DEFAULT_METADATA_ROOT_DIR}: {error}")
    try:
        shutil.rmtree(DEFAULT_SPARQLS_ROOT_DIR)
    except Exception as error:
        logger.error(f"Failed to delete {DEFAULT_SPARQLS_ROOT_DIR}: {error}")


def get_sphn_prefix(schema:str) -> str:
    
    sphn_graph = load_graph(schema, 'turtle')
    sphn_prefix = dict(sphn_graph.namespaces())['sphn']
    return str(sphn_prefix)


def main(db_host: str,
         username: str,
         password: str,
         sphn_schema: str,
         server_type: str = DEFAULT_DB,
         timeout: int = 1800,
         output_dir: str = SCRIPT_DIR,
         generate_sparqls: bool = DEFAULT_GEN_SPARQLS,
         data_source: str = None,
         project_schema: str = None,
         metadata_graph: str = None,
         workers: int = 1,
         terminology_graph: str = None):
    """
    Main entry point for the metadata generator
    :param db_host: database host and port. ex: localhost:7200
    :param username: database username
    :param password: database password
    :param data_source: The name of the repository or dataset to query
    :param timeout: The timeout (in seconds) to apply for SPARQL queries. Default is `1800`.
    :param project_schema: Path to the Project schema .ttl file on disk
    :param server_type: The type of server: only 'fuseki' or 'graphdb' are currently supported. Default is graphdb.
    :param output_dir: The directory to save the output. Default is script directory
    :param generate_sparqls: Flag to generate sparql queries or not. Default is 1 [True]
    :param sphn_schema: Path to the SPHN schema .ttl file on disk. Default is script directory
    :param metadata_graph: Name/iri of graph to query.
    """
    start_time = time()
    # sanitize inputs
    db_host = sanitize(db_host)
    username = sanitize(username)
    password = sanitize(password)
    sphn_schema = sanitize(sphn_schema)
    server_type = sanitize(server_type)
    if data_source:
        data_source = sanitize(data_source)
    if project_schema:
        project_schema = sanitize(project_schema)
    if metadata_graph:
        metadata_graph = sanitize(metadata_graph)
    if terminology_graph:
        terminology_graph = sanitize(terminology_graph)
    if output_dir:
        output_dir = sanitize(output_dir)
    
    
    # check configs
    if db_host.startswith('http'):
        protcol, db_host = db_host.split(':', 1)
        db_host = db_host[2:]

    if server_type == 'graphdb':
        assert data_source, "Please provide a GraphDB repository name to query"

    if server_type == 'fuseki':
        assert data_source, "Please provide a Fuseki Dataset name to query"
        assert metadata_graph, "Please provide a named graph with the data to query"
        assert terminology_graph, "Please provide a named graph with the terminology to query"
    
    try:
        workers = int(workers)
    except Exception as e:
        workers = 1
    
    logger.info(f"Running for Server type: {server_type} with {workers} worker(s)")

    # check if ttl files exist
    for schema in [sphn_schema, project_schema]:
        if schema:
            if not os.path.exists(schema):
                logger.error(f"Schema file not found: {schema}")
                raise FileNotFoundError(f"Schema file not found: {schema}")
            if not schema.endswith(".ttl"):
                logger.error(f"Schema file is not a .ttl file")
                raise ValueError(f"Schema file is not a .ttl file: {schema}")
            
    # extract schema file name as project name
    project_name = extract_project_name(project_schema, sphn_schema)

    # get schema version
    sphn_prefix = get_sphn_prefix(schema=sphn_schema)

    if server_type in ['fuseki', 'graphdb']:
        # Prep SPARQL endpoint
        sparql_endpoint = get_db_connection(db_host=db_host,
                                            username=username,
                                            password=password,
                                            server_type=server_type,
                                            data_source=data_source,
                                            timeout=timeout
                                            )
    else:
        sparql_endpoint = None
    
    if server_type == 'einstein':
        token = get_einstein_api_token(einstein_ip=db_host, username=username, password=password)
       
        logger.info(f"Einstein Token creation time: {EINSTEIN_TOKEN_CREATION_TIME}")
    else:
        token = None

    # Test DB connection, patient data and terminology data available in graph
    test_sparql_endpoint(sparql_endpoint, einstein_token=token, einstein_ip=db_host) 
    test_patient_data(sparql_endpoint, einstein_token=token, einstein_ip=db_host, sphn_prefix=sphn_prefix) 
    test_terminology_data(sparql_endpoint, einstein_token=token, einstein_ip=db_host, sphn_prefix=sphn_prefix)

    # Prep the output directories
    sparqls_dir, metadata_dir = create_output_dirs(project_name)

    try:
        # Generate the sparql queries if required
        logger.info("Generating sparqls")
        terminology_prefixes, prefixes_to_keep = run_sparqler(sphn_schema=sphn_schema,
                    project_schema=project_schema,
                    output_dir=DEFAULT_SPARQLS_ROOT_DIR,
                    metadata=True,
                    metadata_server_type=server_type,
                    metadata_graph=metadata_graph,
                    terminology_graph=terminology_graph,
                    generate_sparqls = generate_sparqls
                    )
        # check that sparqls have been created/exist
        if is_sparqls_ready(str(sparqls_dir)):
            logger.info("Sparqls generation completed")
            # Generate the metadata
            if sparql_endpoint:
                generate_metadata(sparqls_dir, metadata_dir, sparql_endpoint=sparql_endpoint, terminology_prefixes=terminology_prefixes, workers=workers)
            else:
                generate_metadata(sparqls_dir, metadata_dir, einstein_ip=db_host, einstein_token=token, terminology_prefixes=terminology_prefixes, username=username, password=password, workers=workers)
        else:
            raise FileNotFoundError(f"Sparqls have not been correctly generated. Check logs for more info")
        
        combine_files(metadata_dir=metadata_dir)

        # run the sqlite conversion+
        logger.info("converting the results into a database")
        prefixes = terminology_prefixes | prefixes_to_keep
        sqlite_converter.run_sqlite_converter(metadata_dir,
                                              db_name = metadata_dir + os.sep + project_name +'.db', 
                                              table_name = 'project_name', 
                                              namespaces = prefixes_to_keep, #{#'sphn': 'https://biomedit.ch/rdf/sphn-schema/sphn#','xsd': 'http://www.w3.org/2001/XMLSchema#'},
                                              prefixes = prefixes
                                              )

        # zip metadata into single file
        zip_output(project_name, metadata_dir, output_dir)
        delete_sparqls_metadata_dirs()

    except Exception as error:
        # delete_sparqls_metadata_dirs()
        logger.error(f"Metadata Generation Error: {error}")

    logger.info(f"Metadata Generator has finished in {timedelta(seconds=int(time()-start_time))}")


if __name__ == '__main__':

    logger = setup_logger()

    parser = argparse.ArgumentParser(description="Script to generate SPHN Project Metadata")
    parser.add_argument('--db-host', help="DB host & port", default="localhost:7200", required=True)
    parser.add_argument('--username', help="DB username")
    parser.add_argument('--password', help="DB password")
    parser.add_argument('--data-source', help="Repository/Dataset name")
    parser.add_argument('--timeout', help="Timeout (in seconds) to apply for SPARQL queries (Default: `1800`)", default=1800)
    parser.add_argument('--sphn-schema', help="Path to SPHN Schema ttl file", required=True)
    parser.add_argument('--server-type', choices=["fuseki", "graphdb", "einstein"], default=DEFAULT_DB)

    parser.add_argument('--project-schema', help="Path to project schema ttl file", default=None)
    parser.add_argument('--gen-sparqls', choices=[0, 1], default=DEFAULT_GEN_SPARQLS,
                        help="Generate sparql queries", type=int)
    parser.add_argument('--output-dir', default=SCRIPT_DIR,
                        help="Path to to store metadata generated")
    parser.add_argument('--graph', help="Named graph with the data", default=None)
    parser.add_argument('--terminology', help="Named graph with terminologies", default=None)
    parser.add_argument('--workers', help="Number of parallel processes to run", default=1)

    args = parser.parse_args()

    main(sphn_schema=args.sphn_schema,
         project_schema=args.project_schema,
         generate_sparqls=args.gen_sparqls,
         db_host=args.db_host,
         username=args.username,
         password=args.password,
         server_type=args.server_type,
         data_source=args.data_source,
         timeout=args.timeout,
         output_dir=args.output_dir,
         metadata_graph=args.graph,
         terminology_graph=args.terminology,
         workers=args.workers
        )
