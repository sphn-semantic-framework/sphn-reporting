""" This script convert the generated metadata into an sqlite database.
"""

import pandas as pd
import sqlite3
import os
import numpy as np
from rdflib import URIRef


LABEL_REPLACEMENTS = {
        "atc":"ATC",
        "sphn-atc":"ATC",
        "chop":"CHOP",
        "eco":"ECO",
        "sphn-eco":"ECO",
        "edam":"EDAM",
        "sphn-edam":"EDAM",
        "efo":"EFO",
        "sphn-efo":"EFO",
        "emdn":"EMDN",
        "euvoc-currency":"EUVoc Currency",
        "sphn-currency":"EUVoc Currency",
        "genepio":"GenEpiO",
        "sphn-genepio":"GenEpiO",
        "geno":"GENO",
        "sphn-geno":"GENO",
        "hgnc":"HGNC",
        "sphn-hgnc":"HGNC",
        "hp":"HPO",
        "sphn-hp":"HPO",
        "icd-10-gm":"ICD-10-GM",
        "icd-o-3":"ICD-O-3",
        "sphn-icd-o-3":"ICD-O-3",
        "loinc":"LOINC",
        "sphn-loinc":"LOINC",
        "ms":"MSO",
        "sphn-ms":"MSO",
        "ncit":"NCIT",
        "sphn-ncit":"NCIT",
        "ngbo":"NGBO",
        "sphn-ngbo":"NGBO",
        "obi":"OBI",
        "sphn-obi":"OBI",
        "oncotree":"OncoTree",
        "ordo":"ORDO",
        "sphn-ordo":"ORDO",
        "snomed":"SNOMED CT",
        "so":"SO",
        "sphn-so":"SO",
        "ucum":"UCUM",
        "xco":"XCO",
        "sphn-xco":"XCO"}

def process_terminologies(iri: str, labels: dict):

    if ':' in iri:
        prefix = iri.split(':', 1)[0]
        if prefix in labels:
            if prefix in LABEL_REPLACEMENTS:
                return LABEL_REPLACEMENTS[prefix]
            else:
                return prefix
        else:
            return iri 


def replace_prefix(iri: str, prefixes: dict = None) -> str:
    
    if isinstance(iri, URIRef):
        iri = str(iri)
    for i, j in prefixes.items():
        iri = iri.replace(j, f"/{i}:")
    
    return iri.lstrip('/')


def replace_terminology_prefix(iri: str, labels: dict):
    if iri in labels:
            if iri in LABEL_REPLACEMENTS:
                return LABEL_REPLACEMENTS[iri]
    return iri


def create_metadata_record(source, target, path, count, hop_number, is_distinct=False):
    metadata = {'source_iri':  source,
    "target_iri":  target,
    "property":  path,
    "count":  count,
    "hop_number":  hop_number,
    "is_distinct": is_distinct
    }
    return metadata

def parse_terminology_instances(csv_folder: str, prefixes: dict, namespaces: dict) -> pd.DataFrame:

    prefix_set = set(prefixes.items())
    ns_set = set(namespaces.items())
    labels = dict(prefix_set - ns_set)
    count_instances = []
    concepts =  os.listdir(csv_folder)
    for concept in concepts:
        count_metadata = pd.read_csv(os.path.join(csv_folder, concept, 'CSV', f"{concept}.csv"), na_values=["", " "])
        count_metadata = count_metadata.replace('', np.nan)
        
        for ind, row in count_metadata.iterrows():
            columns = list(count_metadata.columns)

            if 'code_codingSystemAndVersion.value' in count_metadata.columns and 'code.value' in count_metadata.columns and not pd.isna(row['code_codingSystemAndVersion.value']) and not pd.isna(row['code.value']):
                #  emit two records if code or terminology is used
                target = replace_terminology_prefix(row['code_codingSystemAndVersion.value'], labels=LABEL_REPLACEMENTS)
                source =  replace_prefix(row['origin.value'], prefixes=prefixes)
                count =  row['count_instances.value']
                path =  replace_prefix(row["path.value"], prefixes=prefixes)
                hop_number = len(path.split('/'))
                metadata1 = create_metadata_record(source, target, path, count, hop_number)
                count_instances.append(metadata1)

                target = replace_terminology_prefix(row['code.value'], labels=LABEL_REPLACEMENTS)
                source =  replace_prefix(row['origin.value'], prefixes=prefixes)
                count =  row['count_instances.value']
                path =  replace_prefix(row["path.value"], prefixes=prefixes)
                hop_number = len(path.split('/'))
                metadata2 = create_metadata_record(source, target, path, count, hop_number)
                count_instances.append(metadata2)
            elif 'code_codingSystemAndVersion.value' in count_metadata.columns and not pd.isna(row['code_codingSystemAndVersion.value']):
                target = replace_terminology_prefix(row['code_codingSystemAndVersion.value'], labels=LABEL_REPLACEMENTS)
                source =  replace_prefix(row['origin.value'], prefixes=prefixes)
                count =  row['count_instances.value']
                path =  replace_prefix(row["path.value"], prefixes=prefixes)
                hop_number = len(path.split('/'))
                metadata = create_metadata_record(source, target, path, count, hop_number)
                count_instances.append(metadata)
            elif 'code.value' in count_metadata.columns and not pd.isna(row['code.value']):
                target = replace_terminology_prefix(row['code.value'], labels=LABEL_REPLACEMENTS)
                source =  replace_prefix(row['origin.value'], prefixes=prefixes)
                count =  row['count_instances.value']
                path =  replace_prefix(row["path.value"], prefixes=prefixes)
                hop_number = len(path.split('/'))
                metadata = create_metadata_record(source, target, path, count, hop_number)
                count_instances.append(metadata)
            else:
                # this is the case, when no target is found. This is already covered by the count queries.
                continue


    return pd.DataFrame(count_instances)    


def parse_count_instances(csv_folder: str, prefixes: dict, namespaces: dict) -> pd.DataFrame:
    
    prefix_set = set(prefixes.items())
    ns_set = set(namespaces.items())
    labels = dict(prefix_set - ns_set)
    count_instances = []
    concepts =  os.listdir(csv_folder)
    for concept in concepts:
        count_metadata = pd.read_csv(os.path.join(csv_folder, concept, 'CSV', f"{concept}.csv"), na_values=["", " "])
        count_metadata = count_metadata.replace('', np.nan)
        
        for ind, row in count_metadata.iterrows():
            columns = list(count_metadata.columns)
            if 'target_concept.value' not in count_metadata.columns or pd.isna(row['target_concept.value']):
                target = None
                path =  None
                hop_number = 0
                source =  replace_prefix(row['origin.value'], prefixes=prefixes)
                count =  row['count_instances.value']
                metadata = create_metadata_record(source, target, path, count, hop_number)
            else:
                target = process_terminologies(row['target_concept.value'], labels=labels)
                path =  replace_prefix(row["path.value"], prefixes=prefixes)
                hop_number = len(path.split('/'))
                source =  replace_prefix(row['origin.value'], prefixes=prefixes)
                count =  row['count_instances.value']
                metadata = create_metadata_record(source, target, path, count, hop_number)
            
            count_instances.append(metadata)

    return pd.DataFrame(count_instances)


def parse_total_count_instances(csv_folder: str, prefixes: dict, namespaces: dict) -> pd.DataFrame:
    prefix_set = set(prefixes.items())
    ns_set = set(namespaces.items())
    labels = dict(prefix_set - ns_set)
    count_instances = []
    concepts =  os.listdir(csv_folder)
    for concept in concepts:
        count_metadata = pd.read_csv(os.path.join(csv_folder, concept, 'CSV', f"{concept}.csv"), na_values=["", " "])
        count_metadata = count_metadata.replace('', np.nan)
        for ind, row in count_metadata.iterrows():
            columns = list(count_metadata.columns)
            target = None
            path =  None
            hop_number = 0
            source = None
            count =  row['count_instances.value']
            metadata = create_metadata_record(source, target, path, count, hop_number)
            
            count_instances.append(metadata)

    return pd.DataFrame(count_instances)


def parse_list_prefix_instances(csv_folder: str, prefixes: dict, namespaces: dict) -> pd.DataFrame:
    prefix_set = set(prefixes.items())
    ns_set = set(namespaces.items())
    labels = dict(prefix_set - ns_set)
    count_instances = []
    concepts =  os.listdir(csv_folder)
    for concept in concepts:
        count_metadata = pd.read_csv(os.path.join(csv_folder, concept, 'CSV', f"{concept}.csv"), na_values=["", " "])
        count_metadata = count_metadata.replace('', np.nan)
        for ind, row in count_metadata.iterrows():
            columns = list(count_metadata.columns)
            target = replace_terminology_prefix(row['code.value'], labels=LABEL_REPLACEMENTS)
            path =  None
            hop_number = 0
            source = None
            count =  row['count_instances.value']
            isDistinct = row['isDistinct']
            metadata = create_metadata_record(source, target, path, count, hop_number, isDistinct)
            
            count_instances.append(metadata)

    return pd.DataFrame(count_instances)


def run_sqlite_converter(metadata_path: str, prefixes: dict, namespaces:dict, db_name: str = 'metadata.db', table_name: str = 'metadata'):
    
    count_metadata_folder = os.path.join(metadata_path, 'count_instances')
    count_metadata = parse_count_instances(count_metadata_folder, prefixes, namespaces)
    terminology_metadata_folder = os.path.join(metadata_path, 'has_terminology')
    terminology_metadata = metadata = parse_terminology_instances(terminology_metadata_folder, prefixes, namespaces)
    list_prefix_metadata_folder = os.path.join(metadata_path, 'list_prefix')
    list_prefix_metadata = metadata = parse_list_prefix_instances(list_prefix_metadata_folder, prefixes, namespaces)
    total_count_metadata_folder = os.path.join(metadata_path, 'total_count')
    total_count_metadata = metadata = parse_total_count_instances(total_count_metadata_folder, prefixes, namespaces)
    metadata = pd.concat([count_metadata, terminology_metadata, list_prefix_metadata, total_count_metadata])

    conn = sqlite3.connect(db_name)
    metadata.to_sql(table_name, con=conn, if_exists='replace', index=False)
    conn.close()


if __name__ == '__main__':
    run_sqlite_converter('metadata/sphn_schema_2024.2',
    #run_sqlite_converter('SPO_data_2024_1_20240704-11-09-2024_23-12-19',
                        db_name = 'sphn_2024.db', 
                        table_name = 'sphn_2024', 
                        namespaces = {'sphn': 'https://biomedit.ch/rdf/sphn-schema/sphn#',
                                      'xsd': 'http://www.w3.org/2001/XMLSchema#'},
                        prefixes= {'brick': 'https://brickschema.org/schema/Brick#',
                                                                    'csvw': 'http://www.w3.org/ns/csvw#',
                                                                    'dc': 'http://purl.org/dc/elements/1.1/',
                                                                    'dcat': 'http://www.w3.org/ns/dcat#',
                                                                    'dcmitype': 'http://purl.org/dc/dcmitype/',
                                                                    'dcterms': 'http://purl.org/dc/terms/',
                                                                    'dcam': 'http://purl.org/dc/dcam/',
                                                                    'doap': 'http://usefulinc.com/ns/doap#',
                                                                    'foaf': 'http://xmlns.com/foaf/0.1/',
                                                                    'geo': 'http://www.opengis.net/ont/geosparql#',
                                                                    'odrl': 'http://www.w3.org/ns/odrl/2/',
                                                                    'org': 'http://www.w3.org/ns/org#',
                                                                    'prof': 'http://www.w3.org/ns/dx/prof/',
                                                                    'prov': 'http://www.w3.org/ns/prov#',
                                                                    'qb': 'http://purl.org/linked-data/cube#',
                                                                    'schema': 'https://schema.org/',
                                                                    'sh': 'http://www.w3.org/ns/shacl#',
                                                                    'skos': 'http://www.w3.org/2004/02/skos/core#',
                                                                    'sosa': 'http://www.w3.org/ns/sosa/',
                                                                    'ssn': 'http://www.w3.org/ns/ssn/',
                                                                    'time': 'http://www.w3.org/2006/time#',
                                                                    'vann': 'http://purl.org/vocab/vann/',
                                                                    'void': 'http://rdfs.org/ns/void#',
                                                                    'wgs': 'https://www.w3.org/2003/01/geo/wgs84_pos#',
                                                                    'owl': 'http://www.w3.org/2002/07/owl#',
                                                                    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                                                                    'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
                                                                    'xsd': 'http://www.w3.org/2001/XMLSchema#',
                                                                    'xml': 'http://www.w3.org/XML/1998/namespace',
                                                                    'chop': 'https://biomedit.ch/rdf/sphn-resource/chop/',
                                                                    'eco': 'http://purl.obolibrary.org/obo/ECO_',
                                                                    'edam': 'http://edamontology.org/',
                                                                    'efo': 'http://www.ebi.ac.uk/efo/EFO_',
                                                                    'emdn': 'https://biomedit.ch/rdf/sphn-resource/emdn/',
                                                                    'genepio': 'http://purl.obolibrary.org/obo/GENEPIO_',
                                                                    'geno': 'http://purl.obolibrary.org/obo/GENO_',
                                                                    'icd-10-gm': 'https://biomedit.ch/rdf/sphn-resource/icd-10-gm/',
                                                                    'loinc': 'https://loinc.org/rdf/',
                                                                    'obi': 'http://purl.obolibrary.org/obo/OBI_',
                                                                    'ordo': 'http://www.orpha.net/ORDO/',
                                                                    'snomed': 'http://snomed.info/id/',
                                                                    'so': 'http://purl.obolibrary.org/obo/SO_',
                                                                    'sphn': 'https://biomedit.ch/rdf/sphn-schema/sphn#',
                                                                    'sphn-atc': 'https://biomedit.ch/rdf/sphn-resource/atc/',
                                                                    'sphn-deprecated': 'https://biomedit.ch/rdf/sphn-ontology/sphn#',
                                                                    'sphn-eco': 'https://biomedit.ch/rdf/sphn-resource/eco/',
                                                                    'sphn-edam': 'https://biomedit.ch/rdf/sphn-resource/edam/',
                                                                    'sphn-efo': 'https://biomedit.ch/rdf/sphn-resource/efo/',
                                                                    'sphn-genepio': 'https://biomedit.ch/rdf/sphn-resource/genepio/',
                                                                    'sphn-geno': 'https://biomedit.ch/rdf/sphn-resource/geno/',
                                                                    'sphn-hgnc': 'https://biomedit.ch/rdf/sphn-resource/hgnc/',
                                                                    'sphn-individual': 'https://biomedit.ch/rdf/sphn-schema/sphn/individual#',
                                                                    'sphn-loinc': 'https://biomedit.ch/rdf/sphn-resource/loinc/',
                                                                    'sphn-obi': 'https://biomedit.ch/rdf/sphn-resource/obi/',
                                                                    'sphn-ordo': 'https://biomedit.ch/rdf/sphn-resource/ordo/',
                                                                    'sphn-so': 'https://biomedit.ch/rdf/sphn-resource/so/',
                                                                    'ucum': 'https://biomedit.ch/rdf/sphn-resource/ucum/',
                                                                     'spo': 'https://biomedit.ch/rdf/sphn-schema/spo#'
                                                                    }
                            )