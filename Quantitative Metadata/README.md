# Instructions for Running the Metadata Generator

The Metadata Generator is a Python script that runs queries on a GraphDB database and returns statistics. The metadata
generator works by first generating SPARQL queries from templates. Then it executes these generated queries against a
GraphDB instance. The output of the script are CSV files that contain count statistics. The metadata generator can work
with both SPHN RDF Schema and project-specific RDF Schema.

## Prerequisites

- Python 3.8+
- Access to DCC GitLab
- GraphDB instance


## Installation

To use the Metadata Generator in a BioMedIT B-Space, you would have to first clone the reporting repository
(https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-reporting). 


```sh
git clone -b main https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-reporting && cd sphn-reporting
```

After which you should create a Python virtual environment:

```sh
python -m venv venv
```

Then activate this newly created virtual environment:

```sh
source venv/bin/activate
```

After which you would have to install the dependencies for the script (a full list of dependencies are provided in
the `requirements.txt`):

```sh
pip install -r requirements.txt
```

Now you should be ready to run the metadata generator script.

## GraphDB Configuration

Since the Metadata Generator script generates and executes SPARQL queries, there can be scenarios where certain queries
are quite large. And this might result in `“QueryBadFormed: A bad request has been sent to the endpoint: probably the
SPARQL query is badly formed”` error.

This is because some of the generated SPARQL queries are too large for the default value of GraphDB's HTTP Header Size.
To bypass this issue, increase the value of the `maxHttpHeaderSize` setting in GraphDB:

```
graphdb.connector.maxHttpHeaderSize=26214400
```

Please contact your RDF Support Working Group members at your node for help in changing the header setting of the
GraphDB instance.


## Run the Metadata Generator

```
usage: main.py [-h]
    --db-host DB_HOST (default: localhost:7200)
    --username USERNAME (Optional)
    --password PASSWORD (Optional)
    --data-source DATA_SOURCE
    --timeout TIMEOUT (default: 1800)
    --sphn-schema SPHN_SCHEMA
    --project-schema PROJECT_SCHEMA (Optional)
    --server-type SERVER_TYPE (default: graphdb)
    --gen-sparqls GEN_SPARQLS (default: 1)
    --output-dir OUTPUT_DIR (Optional)
    --graph GRAPH (Optional)
    --workers WORKERS (default: 1)

Script to generate SPHN Project Metadata

options:
  -h, --help                            show this help message and exit
  --db-host DB_HOST                     Fuseki/GraphDB host & port
  --username USERNAME                   Fuseki/GraphDB username
  --password PASSWORD                   Fuseki/GraphDB password
  --data-source DATA_SOURCE             Repository/Dataset name
  --timeout TIMEOUT                     Timeout (in seconds) to apply for SPARQL queries (Default: `1800`)
  --sphn-schema SPHN_SCHEMA             Path to SPHN Schema ttl file
  --project-schema PROJECT_SCHEMA       Path to project schema ttl file
  --server-type {fuseki,graphdb}
  --gen-sparqls {0,1}                   Generate sparql queries
  --output-dir OUTPUT_DIR               Path to to store metadata generated
  --graph GRAPH                         Graph name/iri to query
  --workers WORKERS                     Number of parallel workers
--
```

Where,
- `DB_HOST` is the URL to the GraphDB instance (without the HTTP in the URL). For example, `localhost:7200`
- `USERNAME` is the username that has sufficient privileges to access and see the GraphDB repository. This is optional
if there are no role-based access control defined for your GraphDB instance
- `PASSWORD` is the password for the username. This is optional if there are no role-based access control defined
for your GraphDB instance
- `DATA_SOURCE` is the name of the GraphDB repository where your data is loaded
- `SPHN_SCHEMA` is the path to the SPHN RDF Schema TTL file
- `PROJECT_SCHEMA` is the path to the Project-specific RDF Schema TTL file. This is optional
- `SERVER_TYPE` defines the type of the GraphDB instance. Allowed values are ‘graphdb’ or ‘fuseki’
- `GEN_SPARQLS` defines whether or not to auto-generate SPARQL queries. A value of 1 (default) indicates that SPARQL
queries will be generated each time the script is run. A value of 0 means that there already exists SPARQL queries at
the right location
- `OUTPUT_DIR` is the output directory to which all the results are written to. This is optional. If there is no output
directory defined then all the output will be written to the current working directory.
- `GRAPH` is the IRI of the Named Graph that should be used for querying. This is optional. If there is no graph
defined then the default graph is queried.
- `TIMEOUT` is the timeout (in seconds) to apply for SPARQL queries (Default: `1800`)
- `WORKERS` is the number of parallel workers to use when running the SPARQL queries (Default: `1`)


## Run the Metadata Generator as a Docker container

To run the Metadata Generator as a Docker container, first pull the Docker image:

```
docker pull registry.dcc.sib.swiss/sphn-semantic-framework/sphn-reporting:latest
```

Then run the image as a container:

```sh
docker run -v .:/data registry.dcc.sib.swiss/sphn-semantic-framework/sphn-reporting:latest \
    --db-host DB_HOST \
    --username USERNAME \
    --password PASSWORD \
    --data-source DATA_SOURCE \
    --timeout TIMEOUT \
    --sphn-schema SPHN_SCHEMA \
    --project-schema PROJECT_SCHEMA \
    --server-type SERVER_TYPE \
    --gen-sparqls GEN_SPARQLS \
    --output-dir OUTPUT_DIR \
    --graph GRAPH \
    --workers WORKERS
```

Here we are mounting the current working directory as the `/data` directory in the container.

Thus all files in the current directory will be accessible via `/data` in the container.


## Generated Statistics

The Metadata Generator creates two types of statistics:
- **Instance counts:** Contains counts of the different instances as observed in the data
- **Terminology counts:** Contains a total count of all the codes that exist for each terminology as observed in the data

The counts are represented in both JSON and CSV for convenience.

The Metadata Generator also creates a SQLite Database file (`*.db`) which is a database representation of the **Instance counts** and **Terminology counts**.


## Reporting of Generated Statistics

Projects are requested to provide the generated statistics (CSV and JSON), the SQLite Database file, and logs as a combined zip archive to the DCC at the time of reporting.

The exact process for reporting is outlined in the reporting template.
